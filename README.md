# workshop_front

```shell
docker build --tag workshop_py:1.0.0 .
docker run -it --expose 5432 -p 5001:5001 -p 5432:5432 workshop_py:1.0.0

docker build -t workshop_db:1.0.0 . 
docker run -it -p 8080:8080 workshop_db:1.0.0

sudo docker run -it --expose 5432 -p 5432:5432 -e  POSTGRES_PASSWORD=password -e POSTGRES_USER=user postgres:latest
```

```shell
https://gitlab.com/Bob-117/epsi_b3_workshop/-/commit/f496e0430dd78018946137cc629a916e2eb16955
https://gitlab.com/Bob-117/epsi_b3_workshop_front/-/commit/771277e4701ea28e498e15de9ae491b6bce9a433
```


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
